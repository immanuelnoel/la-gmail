-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2016 at 09:54 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lagmail`
--

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE IF NOT EXISTS `emails` (
  `emailID` int(11) NOT NULL AUTO_INCREMENT,
  `from` varchar(50) NOT NULL,
  `to` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`emailID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`emailID`, `from`, `to`, `subject`, `content`, `timestamp`) VALUES
(1, 'immanuelnoel@lagmail.com', 'vlad@lagmail.com', 'Test Email', 'OKie. This is a test email', '2016-12-27 09:58:03'),
(6, 'vlad@lagmail.com', 'inoel@lagmail.com, immanuelnoel@lagmail.com, reach@immanuelnoel.com', 'Test 2', 'OKie.. some random email message', '2016-12-28 16:32:21'),
(7, 'vlad@lagmail.com', 'inoel@lagmail.com', 'Test 2', 'Test 2fsfa', '2016-12-28 16:08:36'),
(8, 'immanuelnoel@lagmail.com', 'vlad@gmail.com, inoel@lagmail.com, vlad@lagmail.com', 'Howz the experience? ', 'This whole site duplicates, simulates, demonstrates the email service. I never gave an iota of thought to all the awesome features that GMail provides. Building this app was an experience to remember. Thanks! ', '2016-12-28 16:37:06'),
(9, 'immanuelnoel@lagmail.com', 'vlad@lagmail.com', 'Enhancements', 'List of enhancements not done at the moment - 1) HTML editor for body, encrypting passwords, ', '2016-12-28 16:39:25'),
(10, 'immanuelnoel@lagmail.com', 'vlad@lagmail.com', 'Enhancements', 'List of enhancements not done at the moment - 1) HTML editor for body, encrypting passwords, ', '2016-12-28 16:39:28'),
(11, 'immanuelnoel@lagmail.com', 'Test@test.com', 'fsa', 'asdfsa', '2016-12-28 16:40:44'),
(12, 'vlad@lagmail.com', 'inoel@lagmail.com', 'Test', 'test', '2016-12-28 17:08:24'),
(13, 'vlad@lagmail.com', 'inoel@lagmail.com', 'FWD: Enhancements', '</br><hr></br><h3>Enhancements</h3><p><b>From:</b>immanuelnoel@lagmail.com</p><p><b>To:</b>vlad@lagmail.com</p><p><b>Date:</b>2016-12-28 22:09:28</p><hr><p>List of enhancements not done at the moment - </br> 1) HTML editor for body, encrypting passwords, </p>', '2016-12-28 20:13:10'),
(14, 'vlad@lagmail.com', 'inoel@lagmail.com', 'Test', 'test', '2016-12-28 20:17:49'),
(15, 'vlad@lagmail.com', 'inoel@lagmail.com', 'FWD: FWD: Enhancements', '</br><hr></br><h3>FWD: Enhancements</h3><p><b>From:</b>vlad@lagmail.com</p><p><b>To:</b>inoel@lagmail.com</p><p><b>Date:</b>2016-12-29 01:43:10</p><hr><p></br><hr></br><h3>Enhancements</h3><p><b>From:</b>immanuelnoel@lagmail.com</p><p><b>To:</b>vlad@lagmail.com</p><p><b>Date:</b>2016-12-28 22:09:28</p><hr><p>List of enhancements not done at the moment - </br> 1) HTML editor for body, encrypting passwords, </p></p>', '2016-12-28 20:21:10'),
(16, 'vlad@lagmail.com', 'inoel@lagmail.com', 'FWD: Test', '</br><hr></br><h3>Test</h3><p><b>From:</b>vlad@lagmail.com</p><p><b>To:</b>inoel@lagmail.com</p><p><b>Date:</b>2016-12-29 01:47:49</p><hr><p>test</p>', '2016-12-28 20:22:00'),
(17, 'vlad@lagmail.com', 'inoel@lagmail.com', 'FWD: Test', '<hr><b>Test</b><p><b>From:</b>vlad@lagmail.com</p><p><b>To:</b>inoel@lagmail.com</p><p><b>Date:</b>2016-12-29 01:47:49</p><hr><p>test</p>', '2016-12-28 20:22:58'),
(18, 'vlad@lagmail.com', 'inoel@lagmail.com', 'FWD: Test', '<hr><b>Test</b><p><b>From:</b>vlad@lagmail.com</p><p><b>To:</b>inoel@lagmail.com</p><p><b>Date:</b>2016-12-29 01:47:49</p><hr><p>test</p>', '2016-12-28 20:23:27'),
(19, 'vlad@lagmail.com', '', 'FWD: Test', '<b>Test</b><p><b>From:</b>vlad@lagmail.com</p><p><b>To:</b>inoel@lagmail.com</p><p><b>Date:</b>2016-12-29 01:47:49</p><hr><p>test</p>', '2016-12-28 20:23:43'),
(20, 'vlad@lagmail.com', 'inoel@lagmail.com', 'FWD: Test', '<b>Test</b><p><b>From:</b>vlad@lagmail.com</p><p><b>To:</b>inoel@lagmail.com</p><p><b>Date:</b>2016-12-29 01:47:49</p><hr><p>test</p>', '2016-12-28 20:23:47'),
(21, 'vlad@lagmail.com', 'inoel@lagmail.com', 'FWD: Test', '<b>Test</b><p><b>From:</b>vlad@lagmail.com</p><p><b>To:</b>inoel@lagmail.com</p><p><b>Date:</b>2016-12-29 01:47:49</p><hr><p>test</p>', '2016-12-28 20:29:41'),
(22, 'vlad@lagmail.com', 'inoel@lagmail.com', 'FWD: Test', 'On 2016-12-29 01:47:49, vlad@lagmail.com wrote:</br><p>test</p>', '2016-12-28 20:30:00'),
(23, 'vlad@lagmail.com', 'inoel@lagmail.com', 'FWD: Test', 'On 2016-12-29 01:47:49, vlad@lagmail.com wrote:</br><p>test</p>', '2016-12-28 20:36:30'),
(24, 'vlad@lagmail.com', 'inoel@lagmail.com', 'FWD: Howz the experience? ', 'On 2016-12-28 22:07:06, immanuelnoel@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">This whole site duplicates, simulates, demonstrates the email service. I never gave an iota of thought to all the awesome features that GMail provides. Building this app was an experience to remember. Thanks! </td></table>', '2016-12-28 20:36:48'),
(25, 'vlad@lagmail.com', 'inoel@lagmail.com', 'FWD: FWD: Howz the experience? ', 'On 2016-12-29 02:06:48, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">On 2016-12-28 22:07:06, immanuelnoel@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">This whole site duplicates, simulates, demonstrates the email service. I never gave an iota of thought to all the awesome features that GMail provides. Building this app was an experience to remember. Thanks! </td></table></td></table>', '2016-12-28 20:36:58'),
(26, 'vlad@lagmail.com', 'inoel@lagmail.com', 'FWD: FWD: FWD: Howz the experience? ', 'On 2016-12-29 02:06:58, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">On 2016-12-29 02:06:48, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">On 2016-12-28 22:07:06, immanuelnoel@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">This whole site duplicates, simulates, demonstrates the email service. I never gave an iota of thought to all the awesome features that GMail provides. Building this app was an experience to remember. Thanks! </td></table></td></table></td></table>', '2016-12-28 20:37:07'),
(27, 'vlad@lagmail.com', 'inoel@lagmail.com', 'FWD: FWD: FWD: FWD: Howz the experience? ', '</br>On 2016-12-29 02:07:07, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">On 2016-12-29 02:06:58, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">On 2016-12-29 02:06:48, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">On 2016-12-28 22:07:06, immanuelnoel@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">This whole site duplicates, simulates, demonstrates the email service. I never gave an iota of thought to all the awesome features that GMail provides. Building this app was an experience to remember. Thanks! </td></table></td></table></td></table></td></table>', '2016-12-28 20:37:49'),
(28, 'vlad@lagmail.com', 'inoel@lagmail.com', 'FWD: FWD: FWD: FWD: FWD: Howz the experience? ', '</br>On 2016-12-29 02:07:49, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;"></br>On 2016-12-29 02:07:07, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">On 2016-12-29 02:06:58, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">On 2016-12-29 02:06:48, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">On 2016-12-28 22:07:06, immanuelnoel@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">This whole site duplicates, simulates, demonstrates the email service. I never gave an iota of thought to all the awesome features that GMail provides. Building this app was an experience to remember. Thanks! </td></table></td></table></td></table></td></table></td></table>', '2016-12-28 20:37:57'),
(29, 'vlad@lagmail.com', 'inoel@lagmail.com', 'FWD: FWD: FWD: FWD: FWD: FWD: Howz the experience? ', '</br>On 2016-12-29 02:07:57, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;"></br>On 2016-12-29 02:07:49, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;"></br>On 2016-12-29 02:07:07, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">On 2016-12-29 02:06:58, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">On 2016-12-29 02:06:48, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">On 2016-12-28 22:07:06, immanuelnoel@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">This whole site duplicates, simulates, demonstrates the email service. I never gave an iota of thought to all the awesome features that GMail provides. Building this app was an experience to remember. Thanks! </td></table></td></table></td></table></td></table></td></table></td></table>', '2016-12-28 20:38:03'),
(30, 'vlad@lagmail.com', 'inoel@lagmail.com', 'FWD: FWD: FWD: FWD: FWD: FWD: FWD: Howz the experience? ', '</br>On 2016-12-29 02:08:03, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;"></br>On 2016-12-29 02:07:57, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;"></br>On 2016-12-29 02:07:49, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;"></br>On 2016-12-29 02:07:07, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">On 2016-12-29 02:06:58, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">On 2016-12-29 02:06:48, vlad@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">On 2016-12-28 22:07:06, immanuelnoel@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">This whole site duplicates, simulates, demonstrates the email service. I never gave an iota of thought to all the awesome features that GMail provides. Building this app was an experience to remember. Thanks! </td></table></td></table></td></table></td></table></td></table></td></table></td></table>', '2016-12-28 20:38:08'),
(31, 'vlad@lagmail.com', 'immanuelnoel@lagmail.com', 'Howz the experience? ', '</br>On 2016-12-28 22:07:06, immanuelnoel@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">This whole site duplicates, simulates, demonstrates the email service. I never gave an iota of thought to all the awesome features that GMail provides. Building this app was an experience to remember. Thanks! </td></table>', '2016-12-28 20:48:54'),
(32, 'vlad@lagmail.com', 'inoel@lagmail.com', 'FWD: Enhancements', '</br>On 2016-12-28 22:09:28, immanuelnoel@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">List of enhancements not done at the moment - 1) HTML editor for body, encrypting passwords, </td></table>', '2016-12-28 20:49:59'),
(33, 'vlad@lagmail.com', 'immanuelnoel@lagmail.com', 'Enhancements', '</br>On 2016-12-28 22:09:28, immanuelnoel@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">List of enhancements not done at the moment - 1) HTML editor for body, encrypting passwords, </td></table>', '2016-12-28 20:50:04'),
(34, 'immanuelnoel@lagmail.com', 'vlad@gmail.com, inoel@lagmail.com, vlad@lagmail.com', 'FWD: Howz the experience? ', '</br>On 2016-12-28 22:07:06, immanuelnoel@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">This whole site duplicates, simulates, demonstrates the email service. I never gave an iota of thought to all the awesome features that GMail provides. Building this app was an experience to remember. Thanks! </td></table>', '2016-12-28 20:56:27'),
(35, 'vlad@lagmail.com', '', 'FWD: FWD: Howz the experience? ', '</br>On 2016-12-29 02:26:27, immanuelnoel@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;"></br>On 2016-12-28 22:07:06, immanuelnoel@lagmail.com wrote:</br><table border="0"><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">This whole site duplicates, simulates, demonstrates the email service. I never gave an iota of thought to all the awesome features that GMail provides. Building this app was an experience to remember. Thanks! </td></table></td></table>', '2016-12-29 04:45:06'),
(36, 'vlad@lagmail.com', '', 'FWD: FWD: Howz the experience? ', '', '2016-12-29 04:47:55'),
(37, 'vlad@lagmail.com', '', 'FWD: FWD: Howz the experience? ', '', '2016-12-29 04:48:44'),
(38, 'vlad@lagmail.com', '', 'FWD: FWD: Howz the experience? ', '', '2016-12-29 04:54:52'),
(39, 'vlad@lagmail.com', '', 'FWD: FWD: Howz the experience? ', '', '2016-12-29 04:57:23'),
(40, 'vlad@lagmail.com', 'inoel@lagmail.com', 'FWD: FWD: Howz the experience? ', 'dfsaf', '2016-12-29 10:29:35'),
(41, 'vlad@lagmail.com', '', 'FWD: Howz the experience? ', 'On 2016-12-28 22:07:06, immanuelnoel@lagmail.com wrote:<br><table border="0"><tbody><tr><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">This whole site duplicates, simulates, demonstrates the email service. I never gave an iota of thought to all the awesome features that GMail provides. Building this app was an experience to remember. Thanks! </td></tr></tbody></table>', '2016-12-29 06:00:26'),
(42, 'vlad@lagmail.com', 'immanuelnoel@lagmail.com, inoel@lagemail.com', 'FWD: Howz the experience? ', 'On 2016-12-29 02:26:27, immanuelnoel@lagmail.com wrote:<br><table border="0"><tbody><tr><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;"><br>On 2016-12-28 22:07:06, immanuelnoel@lagmail.com wrote:<br><table border="0"><tbody><tr><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">This whole site duplicates, simulates, demonstrates the email service. I never gave an iota of thought to all the awesome features that GMail provides. Building this app was an experience to remember. Thanks! </td></tr></tbody></table></td></tr></tbody></table>', '2016-12-29 10:49:42'),
(43, 'vlad@lagmail.com', 'inoel@gmail.com', 'FWD: FWD: Howz the experience? ', 'On 2016-12-29 02:26:27, immanuelnoel@lagmail.com wrote:<br><table border="0"><tbody><tr><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;"><br>On 2016-12-28 22:07:06, immanuelnoel@lagmail.com wrote:<br><table border="0"><tbody><tr><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">This whole site duplicates, simulates, demonstrates the email service. I never gave an iota of thought to all the awesome features that GMail provides. Building this app was an experience to remember. Thanks! </td></tr></tbody></table></td></tr></tbody></table>', '2016-12-29 11:41:59'),
(44, 'vlad@lagmail.com', 'inoel@lagmail.com', 'FWD: FWD: Howz the experience? ', 'On 2016-12-29 02:26:27, immanuelnoel@lagmail.com wrote:<br><table border="0"><tbody><tr><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;"><br>On 2016-12-28 22:07:06, immanuelnoel@lagmail.com wrote:<br><table border="0"><tbody><tr><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">This whole site duplicates, simulates, demonstrates the email service. I never gave an iota of thought to all the awesome features that GMail provides. Building this app was an experience to remember. Thanks! </td></tr></tbody></table></td></tr></tbody></table>', '2016-12-29 12:20:01'),
(45, 'vlad@lagmail.com', 'immanuelnoel@lagmail.com', 'FWD: Howz the experience? ', 'On 2016-12-29 02:26:27, immanuelnoel@lagmail.com wrote:<br><table border="0"><tbody><tr><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;"><br>On 2016-12-28 22:07:06, immanuelnoel@lagmail.com wrote:<br><table border="0"><tbody><tr><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">This whole site duplicates, simulates, demonstrates the email service. I never gave an iota of thought to all the awesome features that GMail provides. Building this app was an experience to remember. Thanks! </td></tr></tbody></table></td></tr></tbody></table>', '2016-12-30 07:45:40'),
(46, 'vlad@lagmail.com', 'immanuelnoel@lagmail.com', 'FWD: Howz the experience? ', 'On 2016-12-29 02:26:27, immanuelnoel@lagmail.com wrote:<br><table border="0"><tbody><tr><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;"><br>On 2016-12-28 22:07:06, immanuelnoel@lagmail.com wrote:<br><table border="0"><tbody><tr><td rowspan="2" style="text-align:left;vertical-align:top;padding-left:15px;border-left:solid 1px #9c9c9c;">This whole site duplicates, simulates, demonstrates the email service. I never gave an iota of thought to all the awesome features that GMail provides. Building this app was an experience to remember. Thanks! </td></tr></tbody></table></td></tr></tbody></table>', '2016-12-30 08:08:01'),
(47, 'vlad@lagmail.com', 'inoel@lagmail.com', 'Attachment', 'Test attachment email', '2016-12-30 08:34:36'),
(48, 'vlad@lagmail.com', 'inoel@lagmail.com', 'Attachment', 'dsa', '2016-12-30 08:41:41');

-- --------------------------------------------------------

--
-- Table structure for table `email_assignment`
--

CREATE TABLE IF NOT EXISTS `email_assignment` (
  `emailAssignmentID` int(11) NOT NULL AUTO_INCREMENT,
  `emailID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `emailStatusID` int(11) NOT NULL,
  `emailRead` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`emailAssignmentID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=81 ;

--
-- Dumping data for table `email_assignment`
--

INSERT INTO `email_assignment` (`emailAssignmentID`, `emailID`, `userID`, `emailStatusID`, `emailRead`) VALUES
(1, 1, 1, 2, 0),
(3, 6, 2, 4, 0),
(4, 7, 2, 4, 0),
(5, 7, 3, 4, 1),
(9, 8, 1, 2, 0),
(10, 8, 3, 1, 0),
(11, 8, 2, 1, 0),
(12, 9, 1, 4, 0),
(13, 10, 1, 2, 0),
(14, 10, 2, 1, 1),
(15, 11, 1, 2, 0),
(16, 12, 2, 4, 0),
(17, 13, 2, 4, 0),
(18, 13, 3, 1, 0),
(19, 14, 2, 2, 0),
(20, 14, 3, 1, 0),
(21, 15, 2, 4, 0),
(22, 15, 3, 1, 0),
(23, 16, 2, 4, 0),
(24, 16, 3, 1, 0),
(25, 17, 2, 4, 0),
(26, 17, 3, 1, 0),
(27, 18, 2, 4, 1),
(28, 18, 3, 1, 0),
(29, 19, 2, 4, 0),
(30, 20, 2, 4, 0),
(31, 20, 3, 1, 0),
(32, 21, 2, 4, 0),
(33, 21, 3, 1, 0),
(34, 22, 2, 4, 0),
(35, 22, 3, 1, 0),
(36, 23, 2, 4, 0),
(37, 23, 3, 1, 0),
(38, 24, 2, 2, 0),
(39, 24, 3, 1, 0),
(40, 25, 2, 2, 0),
(41, 25, 3, 1, 0),
(42, 26, 2, 2, 0),
(43, 26, 3, 1, 0),
(44, 27, 2, 2, 0),
(45, 27, 3, 1, 0),
(46, 28, 2, 2, 0),
(47, 28, 3, 1, 0),
(48, 29, 2, 2, 0),
(49, 29, 3, 1, 0),
(50, 30, 2, 2, 0),
(51, 30, 3, 1, 1),
(52, 31, 2, 2, 0),
(53, 31, 1, 1, 0),
(54, 32, 2, 2, 1),
(55, 32, 3, 1, 0),
(56, 33, 2, 2, 0),
(57, 33, 1, 1, 0),
(58, 34, 1, 2, 0),
(59, 34, 3, 1, 1),
(60, 34, 2, 1, 1),
(61, 35, 2, 4, 1),
(62, 36, 2, 4, 0),
(63, 37, 2, 4, 0),
(64, 38, 2, 4, 0),
(65, 39, 2, 4, 1),
(66, 40, 2, 2, 0),
(67, 41, 2, 4, 1),
(68, 40, 3, 1, 1),
(69, 42, 2, 2, 0),
(70, 42, 1, 1, 0),
(71, 43, 2, 2, 1),
(72, 44, 2, 2, 1),
(73, 44, 3, 1, 0),
(74, 45, 2, 4, 1),
(75, 46, 2, 4, 1),
(76, 47, 2, 2, 1),
(77, 47, 3, 1, 0),
(78, 47, 3, 1, 0),
(79, 48, 2, 2, 1),
(80, 48, 3, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `email_attachments`
--

CREATE TABLE IF NOT EXISTS `email_attachments` (
  `emailAttachmentID` int(11) NOT NULL AUTO_INCREMENT,
  `localDestination` varchar(1000) NOT NULL,
  `attachmentURL` text NOT NULL,
  PRIMARY KEY (`emailAttachmentID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `email_attachments`
--

INSERT INTO `email_attachments` (`emailAttachmentID`, `localDestination`, `attachmentURL`) VALUES
(7, 'C:\\Personal\\Work\\Projects\\Gmail\\services\\uploads\\REELASE NOTES.txt', 'http://localhost/gmail/services/uploads/REELASE NOTES.txt'),
(8, 'C:\\Personal\\Work\\Projects\\Gmail\\services\\uploads\\Groot.txt', 'http://localhost/gmail/services/uploads/Groot.txt');

-- --------------------------------------------------------

--
-- Table structure for table `email_attachments_map`
--

CREATE TABLE IF NOT EXISTS `email_attachments_map` (
  `emailAttachmentsMapID` int(11) NOT NULL AUTO_INCREMENT,
  `emailID` int(11) NOT NULL,
  `emailAttachmentID` int(11) NOT NULL,
  PRIMARY KEY (`emailAttachmentsMapID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `email_attachments_map`
--

INSERT INTO `email_attachments_map` (`emailAttachmentsMapID`, `emailID`, `emailAttachmentID`) VALUES
(2, 47, 7),
(4, 48, 8);

-- --------------------------------------------------------

--
-- Table structure for table `email_status`
--

CREATE TABLE IF NOT EXISTS `email_status` (
  `emailStatusID` int(11) NOT NULL AUTO_INCREMENT,
  `Status` varchar(10) NOT NULL,
  PRIMARY KEY (`emailStatusID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `email_status`
--

INSERT INTO `email_status` (`emailStatusID`, `Status`) VALUES
(1, 'Inbox'),
(2, 'Sent'),
(3, 'Drafts'),
(4, 'Trash');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userID`, `username`, `password`) VALUES
(1, 'immanuelnoel', 'immanuelnoel'),
(2, 'vlad', 'vlad'),
(3, 'inoel', 'inoel');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
