<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH.'libraries/REST_Controller.php');

class Lagmail_users extends REST_Controller {
        
    function list_get() {
        
		$query = $this->db->query("select * from users");

		$data['users'] = array();
		foreach ($query->result() as $row)
		{
			$user['userid'] = $row->userID;
			$user['username'] = $row->username;
			
			array_push($data['users'], $user);
		}
		
        $data['description'] = 'Listing all DB users';
        $this->response($data, 200);    
    }   
	
	function login_post() {
        
		$query = $this->db->query("select * from users where username = '" . $this->post('username') . "' and password = '" . $this->post('password'). "'");

		$data['users'] = array();
		foreach ($query->result() as $row)
		{
			$user['userid'] = $row->userID;
			$user['username'] = $row->username;
			
			array_push($data['users'], $user);
		}
		
        $data['description'] = 'Login service';
        $this->response($data, 200); 
    }   
    
}
