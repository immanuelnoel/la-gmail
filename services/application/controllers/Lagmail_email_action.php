<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH.'libraries/REST_Controller.php');

class Lagmail_email_action extends REST_Controller {
	
	function summary_get(){
		
		$query = $this->db->query("select ea.emailStatusID, status, count(*) as count from emails e join email_assignment ea on e.emailID = ea.emailID join email_status es on ea.emailStatusID = es.emailStatusID where ea.userid = " . $this->get('userID') . " and ea.emailRead = 0 group by ea.emailStatusID");
		
		$data['summary'] = $query->result();
        $data['description'] = 'Listing email summary for a user';
        $this->response($data, 200);    
	}	
		
    function listForStatus_get() {
        
		$query = $this->db->query("select * from emails e join email_assignment ea on e.emailID = ea.emailID join email_status es on ea.emailStatusID = es.emailStatusID where ea.userid = " . $this->get('userID') . " and ea.emailStatusID = " . $this->get('emailStatusID') . " order by e.timestamp desc");

		$emails = array();
		foreach ($query->result() as $row)
		{
			$email['emailid'] = $row->emailID;
			$email['userID'] = $row->userID;
			$email['emailAssignmentID'] = $row->emailAssignmentID;
			$email['emailStatusID'] = $row->emailStatusID;
			$email['emailStatus'] = $row->Status;
			$email['from'] = $row->from;
			$email['to'] = $row->to;
			$email['subject'] = $row->subject;
			$email['content'] = substr($row->content, 0, 30) . "...";
			$email['timestamp'] = $row->timestamp;
			$email['emailRead'] = $row->emailRead;
			
			array_push($emails, $email);
		}
		
		$data['emails'] = $emails;
        $data['description'] = 'Listing emails for a user';
        $this->response($data, 200);    
		
    }  

	function details_get() {
		
		// Mark email as read
		$query = $this->db->query("update email_assignment set emailRead = 1 where emailAssignmentID = " . $this->get('emailAssignmentID'));
        
		// Get email with status, assignment
		$query = $this->db->query("select * from email_assignment ea join emails e on e.emailID = ea.emailID join email_status es on ea.emailStatusID = es.emailStatusID where ea.emailAssignmentID = " . $this->get('emailAssignmentID'));
		
		$emailData = $query->result()[0];
		$emailID = $emailData->emailID;
		$data['email'] = $emailData;
        
		// Get email attachments
		$query = $this->db->query("select * from email_attachments ea join email_attachments_map eam on ea.emailAttachmentID = eam.emailAttachmentID where eam.emailID = " . $emailID);
		$data['email']->attachments = $query->result();
		
		$data['description'] = 'Listing email by EmailAssignmentID';
        $this->response($data, 200);    
		
    }
	
	function markUnread_post() {
		
		$query = $this->db->query("update email_assignment set emailRead = 0 where emailAssignmentID = " . $this->post('emailAssignmentID'));
        $data['description'] = 'Marking email as unread by EmailAssignmentID';
        $this->response($data, 200);    
		
    }   	
	
	function deleteEmail_post(){
		
		$data = null;
		if($this->post('emailAssignmentID') > 0) {
		
			$query = $this->db->query("update email_assignment set emailStatusID = 4 where emailAssignmentID = " . $this->post('emailAssignmentID'));
			
			$data['success'] = true;
			$data['message'] = "Email successfully deleted";
		
		} else {
			
			$data['success'] = false;
			$data['message'] = "Email could not be deleted";
			
		}
		
		$data['description'] = "Trash'd email by EmailAssignmentID";
        $this->response($data, 200);   
	}
	
	function deleteAttachment_post(){
		
		$data = null;
		if($this->post('emailAttachmentID') > 0) {
		
			$query = $this->db->query("delete from email_attachments where emailAttachmentID = " . $this->post('emailAttachmentID'));
			$query = $this->db->query("delete from email_attachments_map where emailAttachmentID = " . $this->post('emailAttachmentID'));
			
			$data['success'] = true;
			$data['message'] = "Email attachment successfully deleted";
		
		} else {
			
			$data['success'] = false;
			$data['message'] = "Email attachment could not be deleted";
			
		}
		
		$data['description'] = "Delete email attachment by EmailAttachmentID. Function intended to be called from draft status only";
        $this->response($data, 200);   
	}
	
	function sendEmail_post() {
        
		$data = null;
		
		try {
			
			// Insert into emails
			if($this->post('emailID') == 0) {
				
				$query = $this->db->query("insert into emails (`from`, `to`, `subject`, `content`) values ('" . $this->post('from') . "', '" . $this->post('to') . "', '" . $this->post('subject') . "', '" .$this->post('content') . "')");
				$data['emailID'] = $this->db->insert_id();
				
			} else {
				
				$query = $this->db->query("update emails set `from` = '" . $this->post('from') . "', `to` = '" . $this->post('to') . "', `subject` = '" . $this->post('subject') . "', `content` = '" .$this->post('content') . "' where emailID = " . $this->post('emailID'));
				$data['emailID'] = $this->post('emailID');
			}
			
			// Create email assignments for sender
			if($this->post('emailAssignmentID') == 0) {
				
				$query = $this->db->query("insert into email_assignment (emailID, userID, emailStatusID, emailRead) values (" . $data['emailID'] . ", " . $this->post('userID') . ", 2, 1)");
				$data['emailAssignmentID'] = $this->db->insert_id();
			
			} else {
				
				$query = $this->db->query("update email_assignment set emailID = " . $data['emailID'] . ", userID = " . $this->post('userID') . ", emailStatusID = 2, emailRead = 1 where emailAssignmentID = " . $this->post('emailAssignmentID'));
				$data['emailAssignmentID'] = $this->post('emailAssignmentID');
			}
			
			
			// Create email assignments for recipients
			$undeliveredEmails = "";
			
			$toAddresses = explode(",", $this->post('to'));
			foreach($toAddresses as $address){
				
				$splitAddress = explode("@", trim($address));
				if($splitAddress[1] == "lagmail.com"){
					
					// Get UserID
					$query = $this->db->query("select * from users where username = '" . $splitAddress[0] . "'");
					$userID = $query->result()[0]->userID;
					
					if($userID > 0) {
						
						// Create Assignment
						$query = $this->db->query("insert into email_assignment (emailID, userID, emailStatusID, emailRead) values (" . $data['emailID'] . ", " . $userID . ", 1, 0)");
					
					} else {
						
						// Unable to deliver emails to a invalid La' Gmail
						$undeliveredEmails .= $address . ", ";
					}
				} else {
					
					// Unable to deliver emails outside of La' Gmail
					$undeliveredEmails .= $address . ", ";
				}
			}
			
			$data['message'] = (strlen($undeliveredEmails) > 0 ? "Email could not be sent to the following emails: " . $undeliveredEmails . " The email(s) are not registered with La' Gmail" : "Email Sent");
		
			// Existing attachments, if any, need to be linked with the sent email. Additional emails may exist when emails are forwarded or replied to.
			if($this->post('emailID') == 0) {
				$attachments = json_decode($this->post('existingAttachments'), true);
				for($i = 0; $i < sizeof($attachments); $i++){
					
					$query = $this->db->query("insert into email_attachments_map (emailID, emailAttachmentID) values (" . $data['emailID'] . ", " . $attachments[$i] . ")");
				}
			}
			
			
			// Upload new attachments
			if (empty($_FILES['attachment']['name'])) {
			
				$data['success'] = true;
				$data['emailAttachmentID'] = 0;
				
			} else {
				
				$uploaddir = '/opt/projects/lagmail.inoel.in/services/uploads/';
				$uploadfile = $uploaddir . basename($_FILES['attachment']['name']);
				$uploadURL = "http://lagmail.inoel.in/services/uploads/" . $_FILES['attachment']['name'];

				if (move_uploaded_file($_FILES['attachment']['tmp_name'], $uploadfile)) {
					
					// Register attachment in DB
					$query = $this->db->query("insert into email_attachments (localDestination, attachmentURL) values ('" . $uploadfile . "', '" . $uploadURL . "')");
					$data['emailAttachmentID'] = $this->db->insert_id();
				
					// Create a map to the email and attachment
					$query = $this->db->query("insert into email_attachments_map (emailID, emailAttachmentID) values (" . $data['emailID'] . ", " . $data['emailAttachmentID'] . ")");
				
					$data['success'] = true;
					$data['attachmentLocation'] = $uploadfile;
					$data['attachmentURL'] = $uploadURL;
					$data['attachmentTempLocation'] = $_FILES['attachment']['tmp_name'];
					
				} else {
					
					$data['success'] = false;
					$data['emailAttachmentID'] = 0;
					$data['attachmentLocation'] = $uploadfile;
					$data['attachmentURL'] = "http://lagmail.inoel.in/services/uploads/" . $_FILES['attachment']['name'];
					$data['attachmentTempLocation'] = $_FILES['attachment']['tmp_name'];
					$data['message'] .= ". Attached file could not be uploaded";
					
				}
			}
			
			
			
		} catch (Exception $e) {
			
			$data['success'] = false;
			$data['message'] = $e->getMessage();
		}
		
		$data['description'] = 'Sending Email';
		$this->response($data, 200);    
    }   
	
	
	function saveDraft_post() {
        
		$data = null;
		
		try {
			
			// Insert into emails
			if($this->post('emailID') == 0) {
				
				$query = $this->db->query("insert into emails (`from`, `to`, `subject`, `content`) values ('" . $this->post('from') . "', '" . $this->post('to') . "', '" . $this->post('subject') . "', '" .$this->post('content') . "')");
				$data['emailID'] = $this->db->insert_id();
				
			} else {
				
				$query = $this->db->query("update emails set `from` = '" . $this->post('from') . "', `to` = '" . $this->post('to') . "', `subject` = '" . $this->post('subject') . "', `content` = '" .$this->post('content') . "' where emailID = " . $this->post('emailID'));
				$data['emailID'] = $this->post('emailID');
			}
			
			// Create email assignments
			if($this->post('emailAssignmentID') == 0) {
				
				$query = $this->db->query("insert into email_assignment (emailID, userID, emailStatusID, emailRead) values (" . $data['emailID'] . ", " . $this->post('userID') . ", 3, 1)");
				$data['emailAssignmentID'] = $this->db->insert_id();
			
			} else {
				
				$query = $this->db->query("update email_assignment set emailID = " . $data['emailID'] . ", userID = " . $this->post('userID') . ", emailStatusID = 3, emailRead = 1 where emailAssignmentID = " . $this->post('emailAssignmentID'));
				$data['emailAssignmentID'] = $this->post('emailAssignmentID');
			}
		
			// Existing attachments, if any, need to be linked with the saved email. Additional emails may exist when emails are forwarded. This only needs to be done once when the email is first saved. Subsequent attempts will result in duplicate attachments being added
			if($this->post('emailID') == 0) {
				$attachments = json_decode($this->post('existingAttachments'), true);
				for($i = 0; $i < sizeof($attachments); $i++){
					
					$query = $this->db->query("insert into email_attachments_map (emailID, emailAttachmentID) values (" . $data['emailID'] . ", " . $attachments[$i] . ")");
				}
			}
			
			if (empty($_FILES['attachment']['name'])) {
			
				$data['success'] = true;
				$data['message'] = "Email saved as Draft";
				$data['emailAttachmentID'] = 0;
				
			} else {
				
				$uploaddir = '/opt/projects/lagmail.inoel.in/services/uploads/';
				$uploadfile = $uploaddir . basename($_FILES['attachment']['name']);
				$uploadURL = "http://lagmail.inoel.in/services/uploads/" . $_FILES['attachment']['name'];

				if (move_uploaded_file($_FILES['attachment']['tmp_name'], $uploadfile)) {
				
					// Register attachment in DB
					$query = $this->db->query("insert into email_attachments (localDestination, attachmentURL) values ('" . $uploadfile . "', '" . $uploadURL . "')");
					$data['emailAttachmentID'] = $this->db->insert_id();
				
					// Create a map to the email and attachment
					$query = $this->db->query("insert into email_attachments_map (emailID, emailAttachmentID) values (" . $data['emailID'] . ", " . $data['emailAttachmentID'] . ")");
				
					$data['success'] = true;
					$data['attachmentLocation'] = $uploadfile;
					$data['attachmentURL'] = $uploadURL;
					$data['attachmentTempLocation'] = $_FILES['attachment']['tmp_name'];
					$data['message'] = "Email, with attachment, saved as Draft";
					
				} else {
					
					$data['success'] = false;
					$data['emailAttachmentID'] = 0;
					$data['attachmentLocation'] = $uploadfile;
					$data['attachmentURL'] = "http://lagmail.inoel.in/services/uploads/" . $_FILES['attachment']['name'];
					$data['attachmentTempLocation'] = $_FILES['attachment']['tmp_name'];
					$data['message'] = "Email saved as Draft, but attached file could not be uploaded";
					
				}
			}
			
		} catch (Exception $e) {
			
			$data['success'] = false;
			$data['message'] = $e->getMessage();
		}
		
		$data['description'] = 'Saving email draft';
		$this->response($data, 200);    
	}   
    
}
