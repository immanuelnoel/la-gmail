// On Ready 

var serviceURL = "http://lagmail.inoel.in/services/index.php";
var serviceKey = "";

$( document ).ready(function() {

	// On Start
	if(localStorage.getItem('userID') == undefined){
		
		$('#content').load('views/login.html');
	
	} else {
		
		$('#username').html(localStorage.getItem('username') + "@lagmail.com | " + '<a href="#" id="logout">Logout</a>'); 
		$('#content').load('views/home.html');
	}
	
	$("#logout").click(function(event){
		
		localStorage.removeItem('userID');
		localStorage.removeItem('username');
		
		location.reload();
	});

});

